# covideo19

Quick & dirty screencast program: a local app for uploading + a webserver for
watching. Video only.

![](doc/demo.mp4)


## How does it work?

- initially, you create a webapp on Heroku

- when you want to share a region of your screen:

    - you launch the local app (which sends screencaptures to the webapp)
    - your viewers open the webapp in their browser


## Why not twitch/jitsi/discord/whatever?

Well, you should probably use twitch/jitsi/discord/whatever rather this thing.
But for my very specific need, it brings the following advantages. 

- for the watchers:

    - very simple: they just have to open a public web page


- for the streamer:

    - quite simple: once installed, you just have to launch the local app when you want to stream
    - you can share a region of your screen (just put some windows in that region)
    - works with low upload speed (typically a 800x600 region takes less than 100 KB/s)
    - low latency


## Installation

- [Install on NixOS](doc/nixos.md)

- [Install on Linux](doc/linux.md)


## See also

- [Coder un streamer video en 135 lignes de Haskell et en 1 week-end](https://gitlab.com/juliendehos/talk-2020-lambdalille-covideo19)


