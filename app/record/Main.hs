{-# LANGUAGE BinaryLiterals #-}
{-# LANGUAGE OverloadedStrings #-}

import           Control.Concurrent (threadDelay)
import           Control.Monad (forever, when)
import qualified Data.ByteString as BS
import qualified Data.Text as T
import qualified GI.Gdk as Gdk
import qualified GI.Gtk as Gtk
import           GI.GdkPixbuf.Enums
import           GI.GdkPixbuf.Objects.Pixbuf
import           GI.GObject.Objects.Object
import           GHC.Int
import qualified Network.WebSockets as WS
import           System.Environment (getArgs)
import           Text.Read (readMaybe)

timestep :: Int
timestep = floor (5e6 :: Double)

curPad, curSize0, curSize1 :: Int32
curPad = 2
curSize0 = 12
curSize1 = curSize0 - 2*curPad

createCursor :: IO Pixbuf
createCursor = do
    Just cursorPix0 <- pixbufNew ColorspaceRgb False 8 curSize0 curSize0
    Just cursorPix1 <- pixbufNew ColorspaceRgb False 8 curSize1 curSize1
    pixbufFill cursorPix0 0x000000ff
    pixbufFill cursorPix1 0xffff00ff
    pixbufCopyArea cursorPix1 0 0 curSize1 curSize1 cursorPix0 curPad curPad
    return cursorPix0

initGtk :: IO (Gdk.Device, Gdk.Window)
initGtk = do
    _ <- Gtk.init Nothing
    Just screen <- Gdk.screenGetDefault
    display <- Gdk.screenGetDisplay screen
    seat <- Gdk.displayGetDefaultSeat display
    Just device <- Gdk.seatGetPointer seat
    window <- Gdk.screenGetRootWindow screen
    return (device, window)

main :: IO ()
main = do
    args <- getArgs
    key <- BS.readFile ".covideo19"
    cursorPix <- createCursor
    (device, window) <- initGtk
    case args of
        [ip, portStr, x, y, w, h, q] -> do
            putStrLn $ "connecting " <> ip <> " on port " <> portStr
            let app = clientApp device cursorPix window 
                        (read x) (read y) (read w) (read h) (T.pack q) key
            case readMaybe portStr of
                Nothing -> putStrLn "failed to parse port"
                Just port -> WS.runClient ip port "" app
        _ -> putStrLn "usage: <ip> <port> <x> <y> <w> <h> <quality (0-100)>"

clientApp :: Gdk.Device -> Pixbuf -> Gdk.Window -> Int32 -> Int32 -> Int32
          -> Int32 -> T.Text -> BS.ByteString -> WS.ClientApp ()
clientApp device cursorPix window x y w h q key conn = do
    WS.sendTextData conn key
    forever $ do
        Just pxbuf <- Gdk.pixbufGetFromWindow window x y w h
        (_, xi, yi) <- Gdk.deviceGetPosition device
        when (xi > x && xi < x + w - curSize0 && yi > y && yi < y + h - curSize0)
            $ pixbufCopyArea cursorPix 0 0 curSize0 curSize0 pxbuf (xi-x) (yi-y)
        img <- pixbufSaveToBufferv pxbuf "jpeg" ["quality"] [q]
        WS.sendBinaryData conn (img :: BS.ByteString)
        objectUnref pxbuf
        threadDelay timestep

