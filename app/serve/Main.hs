{-# LANGUAGE OverloadedStrings #-}

import           Control.Monad (forever, guard)
import qualified Data.ByteString.Char8 as BC
import qualified Data.ByteString.Lazy as BS
import           Data.IORef (atomicModifyIORef', IORef, newIORef)
import           Data.Maybe (fromMaybe)
import qualified Data.Text.Lazy as T
import           Data.Time.Clock (diffTimeToPicoseconds, utctDayTime)
import           Data.Time.Clock.POSIX (getCurrentTime)
import           Network.Wai (Application)
import           Network.Wai.Handler.WebSockets (websocketsOr)
import qualified Network.WebSockets as WS
import           System.Environment (lookupEnv)
import qualified Web.Scotty as SC

data Model = Model 
    { _img :: BS.ByteString
    , _freq :: Double
    , _lastTime :: Double
    , _nbConn :: Int
    }

updateImg :: BS.ByteString -> Model -> (Model, ())
updateImg img model = (model { _img = img }, ())

updateConn :: Model -> (Model, BS.ByteString)
updateConn model =
    let nbConn = _nbConn model
        model' = model { _nbConn = 1 + nbConn }
    in (model', _img model)

updateFreq :: Double -> Model -> (Model, Double)
updateFreq t model =
    let dt = t - _lastTime model 
        freq0 = _freq model
        freq1 = (fromIntegral $ _nbConn model) / dt
    in if dt < 1
        then (model, freq0)
        else (model { _nbConn = 0, _lastTime = t, _freq = freq1 }, freq1)

myGetTime :: IO Double
myGetTime = toSec <$> getCurrentTime
    where toSec = (* 1e-12) . fromIntegral . diffTimeToPicoseconds . utctDayTime

wsApp :: IORef Model -> BC.ByteString -> Application -> Application
wsApp modelRef key = websocketsOr WS.defaultConnectionOptions (wsHandle modelRef key)

wsHandle :: IORef Model -> BC.ByteString -> WS.PendingConnection -> IO ()
wsHandle modelRef key pc = do
    conn <- WS.acceptRequest pc
    msg0 <- WS.receiveDataMessage conn
    guard (WS.fromDataMessage msg0 == key)
    forever $ do
        img <- WS.receiveData conn
        atomicModifyIORef' modelRef (updateImg img)

httpApp :: IORef Model -> SC.ScottyM ()
httpApp modelRef = do
    SC.get "/" $ SC.file "static/index.html"
    SC.get "/monitor" $ SC.file "static/monitor.html"
    SC.get "/img" $ do
        SC.addHeader "Content-Type" "image/jpeg"
        img <- SC.liftAndCatchIO (atomicModifyIORef' modelRef updateConn)
        SC.raw img
    SC.get "/conn" $ do
        t <- SC.liftAndCatchIO myGetTime
        freq <- SC.liftAndCatchIO (atomicModifyIORef' modelRef $ updateFreq t)
        let freqI = floor freq
        SC.text $ T.pack $ show (freqI :: Int)

main :: IO ()
main = do
    key <- BC.readFile ".covideo19"
    img0 <- BS.readFile "static/out.jpg"
    t0 <- myGetTime
    modelRef <- newIORef (Model img0 0 t0 0)
    port <- read . fromMaybe "3000" <$> lookupEnv "PORT"
    putStrLn $ "listening port " ++ show port ++ "..."
    SC.scotty port $ do
        SC.middleware (wsApp modelRef key)
        httpApp modelRef

