# Install on Linux

## Set up

- install Docker

- install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli) and
  create an account on [Heroku](https://www.heroku.com/)

- install [Haskell Stack](https://docs.haskellstack.org/en/stable/README/)


## Server

- create a Heroku app, for example `mycovideo19`:

```
heroku login
heroku container:login
heroku create mycovideo19
```

- write a key in `.covideo19`, for example:

```
echo "mykey" > .covideo19
```

- build your docker image and push it to your Heroku app:

```
docker build -t mycovideo19:latest .
docker tag mycovideo19:latest registry.heroku.com/mycovideo19/web
docker push registry.heroku.com/mycovideo19/web
heroku container:release web --app mycovideo19
```

- open a web browser and go to <http://mycovideo19.herokuapp.com> (or
  <http://mycovideo19.herokuapp.com/monitor> for monitoring)


## Client

- build the project (this might take some time):

```
stack build
```

- run `covideo-record`, for example to stream the bottom-right corner of a
  full-HD screen:

```
stack run -- mycovideo19.herokuapp.com 80 1120 480 800 600 25
```

