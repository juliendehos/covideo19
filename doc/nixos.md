# Install on NixOS


## Quickstart

- install Docker and Heroku CLI

- create an account on [Heroku](https://www.heroku.com/)

- write a key in a `.covideo19` file:

```
echo "mykey" > .covideo19
```

- create a Heroku app, for example `mycovideo19`:

```
heroku login
heroku container:login
heroku create mycovideo19
```

- build and deploy your app:

```
nix-build nix/docker.nix
docker load < result
docker tag covideo19:latest registry.heroku.com/mycovideo19/web
docker push registry.heroku.com/mycovideo19/web
heroku container:release web --app mycovideo19
```

- open a web browser and go to <http://mycovideo19.herokuapp.com> (or
  <http://mycovideo19.herokuapp.com/monitor> for monitoring)

- run `covideo-record`, for example to stream the bottom-right corner of a
  full-HD screen:

```
nix-shell --run "cabal run covideo19-record mycovideo19.herokuapp.com 80 1120 480 800 600 25"
```

- shutdown the heroku app:

```
heroku ps:scale web=0 --app mycovideo19
```


## Developer area

- run the server locally:

```
nix-shell --run "cabal run covideo19-serve"
```

- run the screencaster locally:

```
nix-shell --run "cabal run covideo19-record 127.0.0.1 3000 0 0 800 600 50"
```

- test the Docker image:

```
docker run --rm -it -p 3000:3000 covideo19:latest
```

- build and push a Docker image on Docker Hub:

```
docker tag covideo19:latest juliendehos/cicd:covideo19
docker push juliendehos/cicd:covideo19
```

