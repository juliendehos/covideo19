let

  pkgs = import <nixpkgs> {};
  app-src = ../. ;
  app-full = pkgs.haskellPackages.callCabal2nix "covideo19" ../. {};
  app = pkgs.haskell.lib.justStaticExecutables app-full;

in

  pkgs.runCommand "covideo19" { inherit app; } ''
    mkdir -p $out/{bin,static}
    cp ${app}/bin/covideo19-serve $out/bin/
    cp ${app}/bin/covideo19-record $out/bin/
    cp ${app-src}/static/* $out/static/
    cp ${app-src}/.covideo19 $out/
  ''

